// 001 - 15-piece puzzle
// Uses SDL2

#include "sdl2.h"

int App_Init() {
    // SDL component initialisation
    
    if(SDL_Init(app_init_flags) < 0) {
        fprintf(stderr, "Error: Could not initialize SDL: %s\n", SDL_GetError());
        return -1;
    } 
    
    #if APP_IMG_INIT == 1
    if(IMG_Init(app_imginit_flags) < 0) {
        fprintf(stderr, "Error: Could not initialize SDL_image: %s\n",IMG_GetError());
        return -1;
    }
    #endif
    
    #if APP_TTF_INIT == 1
    if(TTF_Init() < 0) {
        fprintf(stderr, "Error: Could not initialize SDL_ttf: %s\n", TTF_GetError());
        return -1;
    }
    #endif
    
    #if APP_MIX_INIT == 1
    if(Mix_Init(app_mixerinit_flags) < 0) {
        fprintf(stderr, "Error: Could not initialize SDL_mixer: %s\n", Mix_GetError());
        return -1;
    }
    #endif
    
    #if APP_NET_INIT == 1
    if(SDLNet_Init() < 0) {
        fprintf(stderr, "Error: Could not initialize SDL_net: %s\n", SDLNet_GetError());
        return -1;
    }
    #endif
    
    
    main_win = SDL_CreateWindow("15 - Piece Puzzle", 
                                SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 
                                APP_DEFSIZE_W, APP_DEFSIZE_H, app_defwindow_flags);
    
    if(!main_win) {
        fprintf(stderr, "Error: Could not create a window: %s\n", SDL_GetError());
        return -1;
    }
    
    main_sf = SDL_GetWindowSurface(main_win);
    
    return 1;
}

int App_Load() {
    // Load media
    return 1;
}

void App_EvLoop() {
    while (SDL_PollEvent(&main_ev) != 0) {
        switch(main_ev.type) {
            // Event handlers
        }
    }
}

void App_Update() {
    // Update Loop
}

void App_Draw() {
    SDL_FillRect(main_sf, NULL, 0x000000);
    SDL_UpdateWindowSurface(main_win);
}

void App_Quit() {
    SDL_FreeSurface(main_sf);
    printf("\n");
    SDL_Quit();
}


int main(int argc, char* argv[]) {
    if(App_Init() < 0) {
        fprintf(stderr, "Error: Could not Initialise program. Exiting\n");
        return 1;
    } else if (App_Load() < 0) {
        fprintf(stderr, "Error: Could not load assets. Exiting\n");
        return 1;
    }
    
    while(app_run) {
        App_EvLoop();
        App_Update();
        App_Draw();
               
    }
    
    App_Quit();
    return 0;
}
