#ifndef GAME_H
#define GAME_H
#include "main.h"
#define GRID_WIDTH 4

enum grid_type {
    GRID_DEFAULT,
    GRID_DEFAULT_IMPOSIBLE, 
    GRID_RANDOM,  // Not implemented yet
    GRID_RANDOM_POSSIBLE // Not implemented yet
};

enum grid_dir {
    UP, DOWN, LEFT, RIGHT
};

typedef struct { 
    unsigned uint8_t data[GRID_WIDTH][GRID_WIDTH]; // The game's 4x4 grid.
    unsigned uint8_t eb_x; // row number of the empty box
    unsigned uint8_t eb_y; // col number of the empty box
} grid_t grid_t;

void InitGrid(uint8_t grid[GRID_WIDTH][GRID_WIDTH], enum grid_type type); // Loads numbers in the grid.
void MovGrid(uint8_t grid[GRID_WIDTH][GRID_WIDTH], enum grid_dir dx);     // Moves elements in the grid.
uint8_t IsSolved(uint8_t grid[GRID_WIDTH][GRID_WIDTH]);                   // Checks whether the grid has been solved or not.

#endif 
