// game.c -- includes func defs for the game.
// It is possible to have variable grid sizes but I will not be implementing
// that to keep things simple, and, you know, its a 15 piece puzzle.

#include "main.h"

/*
 * We are trying to check if the number of inversions i.e. whether a number in
 * a sequence is ahead of a nummber larger than itself for each number, and 
 * checking whether the blank piece (16) is on an even or an odd row, which 
 * are used to determine its solvability (see the wikipedia article for this 
 * puzzle for the solvability condition, or look at the code.)
 */
uint8_t IsSolvable(grid_t grid) {
    unsigned uint8_t ninv = 0;
    for (unsigned uint8_t i = 1; i < GRID_WIDTH*GRID_WIDTH; ++i)
        for (unsigned uint8_t j = 0; j < i; ++j)
            if(grid.data[(int) i/GRID_WIDTH][i%GRID_WIDTH] < grid[(int) i/GRID_WIDTH - 1][i%GRID_WIDTH - 1])
                ++ninv;
    if(grid.eb_x % 2 != 0)
        if(ninv % 2 != 0)
            return 1;
        else 
            return 0;
    else
        if(ninv % 2 != 0)
            return 0;
        else
            return 1;
}

uint8_t IsSolved(grid_t grid) {
    for (unsigned uint8_t i = 0; i < sizeof(grid)/sizeof(grid[0]); ++i)
        for (unsigned uint8_t j = 0; j < sizeof(grid[0])/sizeof(grid[0][0]); ++j)
            if(grid.data[i][j] != i*GRID_WIDTH + j)
                return -1;
    return 1;
}

void InitGrid(grid_t grid, enum grid_type type) {
    switch(grid_type) {
        case GRID_DEFAULT:
            for (unsigned uint8_t i = 0; i < sizeof(grid)/sizeof(grid[0]); ++i)
                for (unsigned uint8_t j = 0; j < sizeof(grid[0])/sizeof(grid[0][0]); ++j)
                    grid.data[i][j] = i*GRID_WIDTH + j;
            grid.eb_x = GRID_WIDTH;
            grid.eb_y = GRID_WIDTH;
            break;
        case GRID_DEFAULT_IMPOSIBLE:
            for (unsigned uint8_t i = 0; i < sizeof(grid)/sizeof(grid[0]); ++i)
                for (unsigned uint8_t j = 0; j < sizeof(grid[0])/sizeof(grid[0][0]); ++j)
                    grid.data[i][j] = i*GRID_WIDTH + j;
            SWAP(grid.data[i][j - 1], grid.data[i][j - 2]);
            grid.eb_x = GRID_WIDTH;
            grid.eb_y = GRID_WIDTH;
            break;
        case GRID_RANDOM:
            break;
        case GRID_RANDOM_POSSIBLE:
            break;
    }
}
/*
 * Type of motion involved:
 * When an arrow key is pressed, the block adjacent to the empty box in the 
 * direction represented by the arrow key is shifted to the blank box.
 * Example:
 * Initial state (X is the empty box):
 * 2 3 X  
 * Final sate when left arrow key is pressed:
 * 2 X 3
 */



void MovGrid(grid_t grid, enum grid_dir dx) {
    switch(dx) {
        case UP:
            if(grid.eb_x == 0)
                break;
            SWAP(grid.data[grid.eb_x][grid.eb_y], grid.data[grid.eb_x - 1][grid.eb_y]);
            --grid.eb_x; // Just to make the step more obvious.
            break;
        case DOWN:
            if(grid.eb_x == GRID_WIDTH)
                break;
            SWAP(grid.data[grid.eb_x][grid.eb_y], grid.data[grid.eb_x + 1][grid.eb_y]);
            ++grid.eb_x; 
            break;
        case LEFT:
            if(grid.eb_y == 0)
                break;
            SWAP(grid.data[grid.eb_x][grid.eb_y], grid.data[grid.eb_x][grid.eb_y - 1]);
            --grid.eb_y; 
            break;
        case RIGHT:
            if(grid.eb_y == GRID_WIDTH)
                break;
            SWAP(grid.data[grid.eb_x][grid.eb_y], grid.data[grid.eb_x][grid.eb_y + 1]);
            ++grid.eb_y;
            break;
    }
}

