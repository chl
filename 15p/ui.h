#ifndef DEF_SDL2_H
#define DEF_SDL2_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#define APP_DEFSIZE_W 400
#define APP_DEFSIZE_H 600

#define APP_IMG_INIT 1
#define APP_TTF_INIT 1
#define APP_MIX_INIT 0
#define APP_NET_INIT 0

#define APP_FPS_CAP 60

// If false, the program loop will stop and it will exit. 
uint8_t app_run = 1;

// Flags go over here. Comment out those which are not required.
int app_init_flags =    
    SDL_INIT_TIMER          | 
    SDL_INIT_AUDIO          | 
    SDL_INIT_VIDEO          | 
 // SDL_INIT_JOYSTICK       | 
 // SDL_INIT_HAPTIC         | 
 // SDL_INIT_GAMECONTROLLER | 
    SDL_INIT_EVENTS         |
    0;


int app_defwindow_flags =   
 // SDL_WINDOW_FULLSCREEN         | 
 // SDL_WINDOW_FULLSCREEN_DESKTOP |
    SDL_WINDOW_OPENGL             | 
 // SDL_WINDOW_HIDDEN             |
 // SDL_WINDOW_BORDERLESS         |
 // SDL_WINDOW_RESIZABLE          |
 // SDL_WINDOW_MINIMIZED          |
 // SDL_WINDOW_MAXIMIZED          |
 // SDL_WINDOW_INPUT_GRABBED      |
 // SDL_WINDOW_ALLOW_HIGHDPI      |
    0;


#if APP_IMG_INIT == 1
int app_imginit_flags =
    IMG_INIT_JPG | 
    IMG_INIT_PNG | 
 // IMG_INIT_TIF |
    0;
#endif

#if APP_MIX_INIT == 1
int app_mixerinit_flags = 
    MIX_INIT_FLAC |
    MIX_INIT_MOD  |
    MIX_INIT_MP3  |
    MIX_INIT_OGG  |
    0;
#endif

SDL_Window *main_win = NULL;
SDL_Renderer *main_rdr = NULL;
SDL_Event main_ev;
SDL_Surface *main_sf = NULL;

int App_Init();    // Initialises stuff.
int App_Load();    // Does thinds to get the window up, like assigning surfaces, renderers, images etc.
void App_Draw();   // The draw part of the loop.
void App_EvLoop(); // The event loop.
void App_Update(); // The screen updater.
void App_Quit();   // Unloads stuff and then quits.

#endif
