#ifndef MAIN_H
#define MAIN_H
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

#define SWAP(a,b) { void *temp = a; a = b; temp = b; }
#endif
